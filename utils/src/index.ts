/**
 * @returns the sum of the numbers
 *
 * @example
 * ```ts
 * console.log(sum(5, 8))
 * ```
 */
export function sum(a: number, b: number) {
  return a + b
}
