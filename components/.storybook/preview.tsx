import {
  ChakraProvider,
  extendTheme,
  Flex,
  Button,
  useColorMode,
  useColorModeValue,
  theme,
} from '@chakra-ui/react'
import { StoryContext } from '@storybook/react'
import * as React from 'react'
import { withPropsTable } from 'storybook-addon-react-docgen'
const { addDecorator } = require('@storybook/react');


export const globalTypes = {
  direction: {
    name: 'Direction',
    description: 'Direction for layout',
    defaultValue: 'LTR',
    toolbar: {
      icon: 'globe',
      items: ['LTR', 'RTL'],
    },
  },
}

const ColorModeToggleBar = () => {
  const { toggleColorMode } = useColorMode()
  const nextMode = useColorModeValue('dark', 'light')

  return (
    <Flex justify="flex-end" mb={4}>
      <Button
        size="md"
        fontSize="lg"
        aria-label={`Switch to ${nextMode} mode`}
        variant="ghost"
        color="current"
        marginLeft="2"
        onClick={toggleColorMode}
      >
        Toggle Color mode
      </Button>
    </Flex>
  )
}

const withChakra = (StoryFn: Function, context: StoryContext) => {
  const { direction } = context.globals
  const dir = direction.toLowerCase()

  return (
    <ChakraProvider theme={extendTheme({ direction: dir })}>
      <div dir={dir} id="story-wrapper" style={{ minHeight: '100vh' }}>
        <StoryFn />
      </div>
    </ChakraProvider>
  )
}



addDecorator(withPropsTable);
export const decorators = [withChakra]

