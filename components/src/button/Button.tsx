import React, { FC } from 'react'
import { Button } from '@chakra-ui/react'

export interface ButtonProps {
  size: 'md' | 'lg'
  label: string
}

export const CustomButton: FC<ButtonProps> = ({ size, label }: ButtonProps) => (
  <Button
    size={size}
    onClick={() => {
      console.log('Clicked')
    }}
  >
    {label}
  </Button>
)

export default CustomButton
