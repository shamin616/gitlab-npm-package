import React from 'react'
import CustomButton from './Button'
import { ChakraProvider } from '@chakra-ui/react'

export default {
  component: CustomButton,
  title: 'CustomButton',
}

const Template = (args: any) => (
  <ChakraProvider>
    <CustomButton {...args} />
  </ChakraProvider>
)

export const Default: any = Template.bind({})
Default.args = {
  // size: 'md',
  // label: 'Hello',
}
